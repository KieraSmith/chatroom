"use strict";

Template.form.events({
    'submit .chat-form': function (event) {
        event.preventDefault();

        if (!Meteor.user()) {
            Materialize.toast('Login to send a message', 4000)
        }

        var messageName = event.currentTarget.children[0].children[0].value;
        Collections.Message.insert({
            username: Meteor.user().username,
            name: messageName,
            timestamp: new Date()
        });

        event.currentTarget.children[0].children[0].value = "";
        return false;
    },

});

Template.message.events({
    'click .delete-text': function (event) {
        Collections.Message.remove({
            _id: this._id
        });
    }

});

Accounts.ui.config({
    passwordSignupFields: "USERNAME_AND_OPTIONAL_EMAIL"
})
