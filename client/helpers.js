"use strict";

Template.message.helpers({
    messageList: function () {
        return Collections.Message.find({});
    },
    formatDate: function (timestamp) {
        var date = new Date(timestamp);
        return date.toLocaleDateString();
    }
});

Router.route('/chat-room/:_id', {
    name: 'chatRoom'
});
